#!/bin/bash

function show_usage {
    echo "Usage :-"
    echo "$0 Argument Options"
    echo ""
    echo "Available Arguments:- "
    echo ""
    echo "  match - Displays match of regex to string"
    echo "  EG :- $0 match \"string\" \"regex\""
    echo ""
    echo "  lswitch :- Switches the Current Level"
    echo ""
    echo "  wmax :- Writes Maximum Files"
    echo ""
    echo "  nsec :- Adds a New Section"
    echo ""
    echo "  ndiff :- Adds a New Difficulty"
    echo "  EG :- $0 ndiff Section_Number"
    echo ""
    echo "  push - Pushes Commits to Origin"
    echo "  Available options :- "
    echo "      normal - Confirms Objects to Commit and Commit Message, and Pushes to Origin"
    echo "      emergency - Commits Everything With a commit message if specified, and pushes to Origin"
    echo "      EG:- $0 push emergency commit_msg"
    exit 99
}

function write_max {
    echo $(expr $(ls -d Levels/*/ -1 | wc -l) - 1) > Levels/m_sec # Writes maximum section file
    if [ "$1" = "push" ]; then
        git add Levels/m_sec
    fi

    # Writes Maximum Difficulty Files
    for d in Levels/*/ ; do
        echo $(expr $(ls -d $d*/ -1 | wc -l) - 1) > ${d}m_diff
        if [ "$1" = "push" ]; then
            git add ${d}m_diff
        fi
    done
    
    # Writes Maximum Level Files
    for d in Levels/*/ ; do
        for g in $d*/ ; do
            echo $(expr $(ls $g -1 | wc -l) - 3) > ${g}m_lvl
            if [ "$1" = "push" ]; then
                git add ${g}m_lvl
            fi
        done 
    done

    if [ "$1" = "push" ]; then
        echo 1 > Progress\ File.txt
    fi
}

if [ $# -lt 1 ]; then
    show_usage
else
    if [ "$1" = "match" ]; then
        python dev/match-regex.py "$2" "$3"
    elif [ "$1" = "wmax" ]; then
        write_max
    elif [ "$1" = "lswitch" ]; then
        python dev/l-switch.py
    elif [ "$1" = "nsec" ]; then
        cp -R dev/templates/Section Levels/$(ls -d Levels/*/ -1 | wc -l)
        write_max
    elif [ "$1" = "ndiff" ]; then
        if [ $# -lt 2 ]; then
            show_usage
        fi
        if [ -d "Levels/$2" ]; then
            cp -R dev/templates/Difficulty Levels/$2/$(ls -d Levels/$2/*/ -1 | wc -l)
            write_max
        else
            echo "ERROR Fatal : Section Does Not Exist !"
        fi
    elif [ "$1" = "push" ]; then
        if [ "$2" = "normal" ]; then
            write_max
            git status
            read -p 'Are all files to be pushed staged? (y/n) ' Roger0
            if [ "$Roger0" = "y" ]; then
                git commit
                git push
            else
                echo "Exiting Now"
                exit 99
            fi
        elif [ "$2" = "emergency" ]; then
            write_max
            git add *
            if [ $# -ge 2 ]; then
                git commit -am "$2"
            else
                git commit -m "Update"
            fi
            git push
        else
            show_usage
        fi
    else
        show_usage
    fi
fi