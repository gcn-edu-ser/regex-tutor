import bullet
from os import system

print("Loading Progres File...\n")
progress = open('Progress File.txt').read().splitlines()

# Default Data Variables
username = "Alpha" # Name of User
c_level = 0 # Currenet Level
c_sec = 0 # Current Section
c_diff = 0 # Current Difficulty

# Save User Data
def saveData() :
    global progress
    print("Saving Data....")
    progress = []
    progress.append(str(0)) # Not First Run
    progress.append(str(c_sec)) # Current Section
    progress.append(str(c_diff)) # Current Difficulty
    progress.append(str(c_level)) # Current Level
    progress.append(str(username)) # Username
    open('Progress File.txt', 'w').write('\n'.join(progress))
    print("Data has been Saved !\n")

# Load User Data
def loadData() :
    global c_sec, c_diff, c_level, username
    c_sec = int(progress[1])
    c_diff = int(progress[2])
    c_level = int(progress[3])
    username = str(progress[4])

if (int(progress[0]) == 1):
    print("No Data in Progress File. Writing Defaults...")
    saveData()
else :
    loadData()


cli_main = bullet.Bullet(
    "What do you want to change?",
    choices = ['Section', 'Difficulty', 'Level', 'Username', 'Save', 'Quit'],
    bullet = ">",
    margin = 2,
)

cli_sec = bullet.Bullet(
    "Choose Section",
    choices = [ str(x) for x in range(0, (int(open("Levels/m_sec").read(1)) + 1))],
    bullet = ">",
    margin = 2,
)

stay = True

while stay:
    system('clear')
    print("Current :\nSection:", c_sec, "\nDifficulty:", c_diff, "\nLevel:", c_level, "\nUsername:", username, "\n")
    result = cli_main.launch()
    if ( result == "Section" ) :
        system('clear')
        c_sec = cli_sec.launch()
    elif ( result == "Difficulty" ) :
        system('clear')
        cli_diff = bullet.Bullet(
            "Choose Difficulty",
            choices = [ str(x) for x in range(0, (int(open("Levels/" + str(c_sec) + "/m_diff").read(1)) + 1))],
            bullet = ">",
            margin = 2,
        )
        c_diff = cli_diff.launch()
    elif ( result == "Level" ) :
        system('clear')
        cli_lvl = bullet.Bullet(
            "Choose Level",
            choices = [ str(x) for x in range(0, (int(open("Levels/" + str(c_sec) + "/" + str(c_diff) + "/m_lvl").read(1)) + 1))],
            bullet = ">",
            margin = 2,
        )
        c_level = cli_lvl.launch()
    elif ( result == "Username" ) :
        system('clear')
        username = input("Username : ")
    elif ( result == "Save" ) :
        saveData()
        stay = False
    elif ( result == "Quit" ) :
        stay = False