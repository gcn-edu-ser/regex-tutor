import re # Module for Regular Expressions
import sys # Module for fetching system arguments
import colorama # Module for color Output

'''
This is an interactive engine for regex. This part of the program takes 2 inputs, 1 regex string, and 1 plain string, and performs an operation on them.
'''

# Color Definitions

green = colorama.Fore.GREEN
yellow = colorama.Fore.YELLOW
blue = colorama.Fore.BLUE
red = colorama.Fore.RED

# Error Dialogs

ver_dialog = 'Your version of python is below the requirement. Please use Python 3+'

arg_dialog = red + '\nPlease make sure you only have 2 inputs.\n 1st input should be a string\n 2nd input should be \'regex\' (Quotes are required)' # Error dialog for wrong arguments

# Testing Requirements

assert sys.version_info >= (3,0), ver_dialog
assert len(sys.argv) == 3, arg_dialog 

# Program

def Main() :
    '''
    Main Function
    '''
    inputs = sys.argv[1:] # Collects arguments into inputs

    expression = re.compile(inputs[1]) # Compiles regular expression

    # Output Print Dialogs
    
    print(green + "======\nOUTPUT\n======")
    print(blue + "Input String: ", yellow +  inputs[0])
    print(blue + "Regular Expression: ", yellow + str(expression))
    print(blue + "Result: ", yellow + str([x.group() for x in re.finditer(expression, str(inputs[0]))])) # Prints list of all matches
    print(red + "\nIf the matching wasn't as expected, please ensure your regex is the 2nd argument and it is surrounded by single quotes")

Main()
