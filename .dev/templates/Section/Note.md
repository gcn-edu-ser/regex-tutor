# Diff.md

This directory seperates the exercises into
- Tutorial
- Easy
- Abstract

For the ease of code they are represented with integers starting from 0. You will find the table below.

|Integer|Difficulty|
|-------|----------|
|0|Tutorial|
|1|Easy|
|2|Abstract|
