# Levels.md

For the ease of code, all the files are named by integers. A translation table is available below.

|Number|Level|
|-|-|
|0|Introduction to Regex-Tutor|
|1|Regex Basic|
|2|MetaCharacters Introduction|
|3|Fullstop|
|4|Character Sets|
|5|Special Character Sets {Shorthands}|
|6|Quantifiers|
