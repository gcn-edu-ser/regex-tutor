# Levels.md

This directory is called levels, for the sake of understanding, but in the actual code. This is what is known as the section.

For the sake of ease of code, the sections are referred to using integers starting from 0, the table below shows what each of the sections mean.

|Integer|Section|
|-------|-------|
|0|Introduction *{Basic Matching, Direct Matching}*|

The `m_sec.txt` file keeps track of what the maximum section you can reach is.
