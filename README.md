# Regex-Tutor

Regex-Tutor is Part of the GComputeNerd Education Series. It is the first project in the Series, and is originally hosted on gitlab. The goal of the Education Series is to make the learning of various topics, and skills easier. You may think of each of these as a course, and most course will have a different style of teaching, although some of them may use the same style. Regex-Tutor is taught tutor style. More information on the GComputeNerd Education Series is available [here](https://gitlab.com/gcn-edu-ser/)

## A Little Info

Below you can find some details about Regex-Tutor

### What is Regex Tutor?

Regex Tutor, is a tutor type CLI application that teaches regex, written in python.

### How does Regex Tutor Work?

Regex tutor is a CLI tool, you run the python script, and there are 2 phases. One where you learn the concepts, and the next tests your application. This course is based on this wonderful regex tutorial by Michelle Fullwood, which you can find [here](https://github.com/michelleful/RegexTutorial)

It lays out the general working of regex at the start, and then introduces various concepts. After a concept is introduced, it is followed by some exercises. These exercises start of directly, and gets increasingly abstract, until it gives you a very abstracted application of regex. The exercises are one of the main functions in this approach, and at the end of the whole course there is a mega test.

For this mega test, it is recommended to get some coffee, get some snacks, and finish all of the questions in one go. Think of this like your final exam. It questions you on everything, and asks all kinds of questions from direct to extremely abstracted. Your final score is displayed at the end. You can't really screenshot it and keep it on a resume cuz anyone can easily fake their score, but if you were sincere and you followed through on the exercises of all the earlier concepts. Then, even though you didn't get a certificate you can flex on your resume. You have now learnt and understood regex, and that was the whole point of this. You should now be able to apply regex in your own situations, and now you too are one of those legends in the development department who does regex.

### Who is this meant for?

Anyone really ! Using the tool doesn't have any knowledge prerequisites, it explains how you will encounter regex, and it is programmed in python, but you don't even need to know how to print text in python to use this tool. Although, contributing to regex-tutor may require some python. Refer to [Contributing.md](CONTRIBUTING.md) for details on how you can contribute.

### What is the goal of this project?

This project was made to help people learn Regex. The whole point of this project was so that learning regex could be more fun, and more easy, and if this helps. Then, I'm very happy to have developed this tutor. *~GComputeNerd*

## Getting Started

Follow the instructions given below to have a working setup of regex-tutor.

### Prerequisites

This project uses python 3, and was developed in Python 3.7.4 . Python 2 is not supported, so you will have to use python 3, and the modules used are listed below.

- regex (For Regular Expressions)

### Installing

If you have all the dependencies above, then all you have to do is run the following commands.

```
git clone https://gitlab.com/gcn-edu-ser/regex-tutor.git

cd regex-tutor

./setup.sh
```

The setup tool will then configure regex-tutor, to your preferred settings.

That's it, the setup script will set up the tutor. You may also use one of our packaged versions, though these are not available yet but are planned for distribution from V1.0 :)

### Using Regex Tutor

1. Depending on how you installed Regex Tutor, run the main script with the name `regex-tutor.py`.
1. You will get a prompt, the initial one shows you general regex working, and then it goes to specific stuff, and after every few concepts you get exercises.
1. That's it, the usage of Regex Tutor is intuitive, but if you do have any issues or requests. You may open an issue on gitlab, and we'll look into it.

## Contributing

If you wish to make some contributions to Regex Tutor, just fork this repo, make your changes and send a pull request. The specifics are explained in [CONTRIBUTING.md](CONTRIBUTING.md) Please refer to that file for some guidelines, and other details like that. Don't worry, that's made for humans too :D

But CONTRIBUTING.md describes how to contrbute **code or documentation**, but that's not the only way you can contribute. You can also contribute by

- Sharing issues, or problems you encountered while using Regex Tutor.
- Sharing Feature Requests that you think would be nice.

If the project helped you please do share it. It would mean a lot if you did.

## Acknowledgements

A full list of contributors and acknowledgements is available in [Acknowledgements.md](Acknowledgements.md)

_**EOF**_
