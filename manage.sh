#!/bin/bash

function show_usage {
    echo "Usage: $0 Arguments Options"
    echo "Available Types :"
    echo "FOR USERS"
    echo "---------"
    echo "      setup - Configures Upstream Branch"
    echo "      update - Updates Code from Upstream Branch"
    echo "      dev - Toggles Developer Mode"
    exit 1
}

if [ $# -lt 1 ]; then
    show_usage
else
    if [ "$1" = "setup" ]; then
        git remote add upstream https://gitlab.com/gcn-edu-ser/Regex-Tutor.git
    elif [ "$1" = "update" ]; then
        git fetch upstram
        git checkout master
        git merge upstream/master
    elif [ "$1" = "dev" ]; then
        if [ -d dev ]; then
            mv dev.sh .dev.sh
            mv dev/ .dev/
            echo "Developer Disabled"
        else
            mv .dev.sh dev.sh
            mv .dev/ dev/
            echo "Developer Enabled"
        fi
    fi
fi