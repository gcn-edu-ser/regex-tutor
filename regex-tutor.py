from pyfiglet import Figlet
from os import system
import re
import bullet
import colorama
import sys

# Color Definitions
greenf = colorama.Fore.GREEN
redf = colorama.Fore.RED
bluef = colorama.Fore.BLUE
yellowf = colorama.Fore.YELLOW

# Error Dialogs
ver_dialog = "Your version of python is below requirement. Please use Python 3+"

# Checking compatibility
assert sys.version_info >= (3,0) , ver_dialog

# Objects To Use
figlet = Figlet(font='slant') # Figlet Renderer
progress = open('Progress File.txt').read().splitlines() # Opens Progress File Fore Reading & Writing

# Default Data Variables
username = "Alpha" # Name of User
c_level = 0 # Currenet Level
c_sec = 0 # Current Section
c_diff = 0 # Current Difficulty
m_level = 0 # Maximum Level
m_diff = 0 # Maximum Difficulty
m_sec = int(open("Levels/m_sec").read(1)) # Maximum Section
level = None # Stores The Level

# Initialization
colorama.init() # Initializes Colorama
system('clear') # Clear Screen

# Banner of Program
def Banner() : # PS This ain't about Hulk. This is the banner at startup
    print(greenf + figlet.renderText('REGEX-TUTOR'), end="")
    print(yellowf + "\tCreated By : ", bluef + "\tAfras Ashraf (@GComputeNerd)")
    print(yellowf + "\tVersion : ", redf + "\tV.0.1.4 Closed Pre Devel")
    print(yellowf + "\tRepository : ", bluef + "\thttps://gitlab.com/gcn-edu-ser/regex-tutor/")
    print(redf + "\t\tLet's have some fun !")
    print(greenf + "-----------------------------------------------------------------")

# Load User Data
def loadData() :
    global c_sec, c_diff, c_level, username, m_level, m_diff
    c_sec = int(progress[1])
    c_diff = int(progress[2])
    c_level = int(progress[3])
    username = str(progress[4])
    m_diff = int(open("Levels/" + str(c_sec) + "/m_diff").read(1))
    m_level = int(open("Levels/" + str(c_sec) + "/" + str(c_diff) + "/m_lvl").read(1))

# Save User Data
def saveData() :
    global progress
    print("Saving Data....")
    progress = []
    progress.append(str(0)) # Not First Run
    progress.append(str(c_sec)) # Current Section
    progress.append(str(c_diff)) # Current Difficulty
    progress.append(str(c_level)) # Current Level
    progress.append(str(username)) # Username
    open('Progress File.txt', 'w').write('\n'.join(progress))
    print("Data has been Saved !")

# Get The Level
def getLevel() :
    global level
    level = open("Levels/" + str(c_sec) + "/" + str(c_diff) + "/" + str(c_level)).read().splitlines()

# Set Color
def setColor(color) :
    if (color == "R") :
        print(redf, end="")
    elif (color == "Y") :
        print(yellowf, end="")
    elif (color == "B") :
        print(bluef, end="")
    elif (color == "G") :
        print(greenf, end="")

# Verifies Regex
def verifyRegex(text, regex, expected_out) :
    expression = re.compile(regex)
    output = str([x.group() for x in re.finditer(expression, text)])
    print(yellowf + "Output : ", output)
    print(redf + "Result...", end="")
    if (output == expected_out) :
        return True
    else :
        return False

# Handles Input
def handleInput(entry, c_line) :
    if (entry == '!help') :
        setColor("Y")
        print("!help - Displays list of Special Commands")
        print("!quit - Quits Regex Tutor")
        print("!continue - Continues Session")
        return c_line-2
    elif (entry == '!quit') :
        print(redf + "Roger!")
        closeMain()
    else :
        return c_line

# Load Level
def loadLevel() :
    system("clear")
    print(greenf + "Level loaded from Levels/" + str(c_sec) + "/" + str(c_diff) + "/" + str(c_level))
    if(str(level[0]) == "#T") :
        print(figlet.renderText(str(level[1])))
        last_line = len(level) # Get last line
        c_line = 2 # Current Line
        while (c_line < last_line) :
            if (level[c_line][0] == "S") :
                setColor("G")
                cli = bullet.YesNo(prompt = "Would you like to start this session? ")
                result = cli.launch()
                if (result == False) :
                    closeMain()
                print("")
            elif (level[c_line][0] == "T") :
                setColor(str(level[c_line][1]))
                c_line = c_line + 1
                print(str(level[c_line]))
            elif (level[c_line][0] == "Y") :
                setColor(str(level[c_line][1]))
                cli = bullet.YesNo(prompt="Continue? ")
                result = cli.launch()
                if (result == True) :
                    print(greenf + "Okay !")
                    print("")
                else :
                    loadLevel()
            elif (level[c_line][0] == "I") :
                setColor(str(level[c_line][1]))
                c_line = c_line + 1
                entry = input(level[c_line] + ' : ')
                c_line = handleInput(entry, c_line)
                print("")
            elif (level[c_line][0] == "R") :
                setColor(str(level[c_line][1]))
                regex = input("Regex : ")
                if (regex[0] == '!') :
                    c_line_old = c_line
                    c_line = handleInput(regex, c_line)
                    if (c_line_old == c_line ) : 
                        c_line = c_line + 1
                        text = level[c_line]
                        c_line = c_line + 1
                        expected_out = level[c_line]
                        result = verifyRegex(text, regex, expected_out)
                        if (result == True) :
                            print(greenf + "Correct")
                        else :
                            print(redf + "Wrong")
                            c_line = c_line - 3
                else :
                    c_line = c_line + 1
                    text = level[c_line]
                    c_line = c_line + 1
                    expected_out = level[c_line]
                    result = verifyRegex(text, regex, expected_out)
                    if (result == True) :
                        print(greenf + "Correct")
                    else :
                        print(redf + "Wrong")
                        c_line = c_line - 3
                print("")
            elif (level[c_line][0] == "N") :
                print("")
            elif (level[c_line][0] == "E") :
                setColor("R")
                cli = bullet.YesNo(prompt = "Next Level? ")
                result = cli.launch()
                if (result == True) :
                    print(greenf + "Roger")
                    nextLevel()
                    saveData()
                    Main()
                    break
                else :
                    print(redf + "Roger")
                    nextLevel()
                    closeMain()
            c_line = c_line + 1
    elif (str(level[0]) == "#P") :
        #Custom Program
        system("python Levels/" + str(c_sec) + "/" + str(c_diff) + "/" + str(c_level))
        nextLevel()
        cli = bullet.YesNo(prompt = "Next level? ")
        result = cli.launch()
        if (result == True) :
            print(greenf + "Roger")
            nextLevel()
            saveData()
            Main()
        else :
            print(redf + "Roger")
            nextLevel()
            closeMain()
    elif (str(level[0]) == "#E") :
        print(figlet.renderText(str(level[1])))
        last_line = len(level)
        c_line = 2
        while (c_line < last_line) :
            if(level[c_line][0] == "S") :
                setColor("G")
                cli = bullet.YesNo(prompt = "Would you like to start this session ")
                result = cli.launch()
                if (result == False) :
                    closeMain()
                print("")
            elif (level[c_line][0] == "T") :
                setColor(str(level[c_line][1]))
                c_line = c_line + 1
                print(str(level[c_line]))
            elif (level[c_line][0] == "H") :
                c_line = c_line + 1
                tempString = level[c_line].split("=]=")
                for i in tempString :
                    if (i != "") :
                        setColor(str(i[0]))
                        print(str(i[1:]), end="")
                print("")
            elif (level[c_line][0] == "N") :
                print("")
            elif (level[c_line][0] == "R") :
                setColor(str(level[c_line][1]))
                regex = input("Regex : ")
                if (regex[0] == '!') :
                    c_line_old = c_line
                    c_line = handleInput(regex, c_line)
                    if (c_line_old == c_line ) : 
                        c_line = c_line + 1
                        text = level[c_line]
                        c_line = c_line + 1
                        expected_out = level[c_line]
                        result = verifyRegex(text, regex, expected_out)
                        if (result == True) :
                            print(greenf + "Correct")
                        else :
                            print(redf + "Wrong")
                            c_line = c_line - 3
                else :
                    c_line = c_line + 1
                    text = level[c_line]
                    c_line = c_line + 1
                    expected_out = level[c_line]
                    result = verifyRegex(text, regex, expected_out)
                    if (result == True) :
                        print(greenf + "Correct")
                    else :
                        print(redf + "Wrong")
                        c_line = c_line - 3
                print("")
            elif (level[c_line][0] == "E") :
                setColor("R")
                cli = bullet.YesNo(prompt = "Next Level? ")
                result = cli.launch()
                if (result == True) :
                    print(greenf + "Roger")
                    nextLevel()
                    saveData()
                    Main()
                    break
                else :
                    print(redf + "Roger")
                    nextLevel()
                    closeMain()
            c_line = c_line + 1

# Update Level
def nextLevel() :
    global c_sec, c_diff, c_level, m_level
    if ( c_level < m_level ) :
        # If current level is not the maximum level
        c_level = c_level + 1 # Go to Next level
    elif ( c_level == m_level ) :
        # If current level is the maximum level
        if ( c_diff < m_diff ) :
            # If the current difficulty is not the highest
            c_diff = c_diff + 1 # Go to next difficulty
            c_level = 0
        elif ( c_diff == m_diff ) :
            # If the current difficulty is the highest
            c_sec = c_sec + 1
            if ( c_sec < (m_sec + 1) ) :
                c_diff = 0
                c_level = 0
            else :
                system("clear")                                                                                                                                                                  
                Banner()                                                                                                                                                                         
                setColor("G")                                                                                                                                                                    
                print("\tCongratulations!")                                                                                                                                                    
                print("\tYou have completed Regex-Tutor. More levels are being worked on, and the final exam will be released after the levels are done!\n\tNow, if you wish you may go to our gitlab repository and contribute and maybe consider donating if this program helped you out! :D")                                                                                                
                print("\tPlease look forward to more programs as part of the GComputeNerd Education Series. I hope I'll see you at the next one")                                              
                print(redf + "\tAdios! ~Afras Ashraf (@GComputeNerd)")
                exit()
    m_level = int(open("Levels/" + str(c_sec) + "/" + str(c_diff) + "/m_lvl").read(1))

# Main Program Function
def Main() :
    getLevel()
    loadLevel()

# Close Program Function
def closeMain() :
    colorama.deinit() # De Initialize Colorama

    # Save Data
    saveData()

    # Output Message
    print("\t\tThanks for using Regex Tutor")
    print(bluef + "\t\tHave a good Day ! :D")
    exit()
# Program Execution Starts Here !

Banner()

# First Run Check
print(redf + "Is First Run? : ", end="")

if (int(progress[0]) == 1 ) :
    # If it is the First Run
    print(greenf + "YES")
    print(redf + "Writing Defaults...")

    # Prompting if user wants to begin setup
    print(bluef, end="")
    cli = bullet.YesNo(prompt = "Begin Setup? ")
    result = cli.launch()

    if ( result == True ) :
        # If setup should be run
        print(redf + "Running Setup..")

        # Setup
        print(yellowf, end="")
        cli = bullet.Input(prompt = "What is your username? ")
        username = cli.launch()
        saveData()
        print(greenf + "Setup Succesful!!\nWelcome", username, ", The World of Regex Awaits!")
        print(bluef, end="")
        cli = bullet.YesNo(prompt = "Shall We Begin? ")
        result = cli.launch()
        if (result == True ) :
            print(greenf + "Roger That !")
        else :
            print(redf + "Thanks for using regex-tutor!\n", end="")
            exit()
    else :
        print(redf + "\t\tThanks for using regex-tutor!\n", end="")
        exit()
else :
    # If It is not the first run
    print(redf + "NO")
    print("Loading Save File")
    loadData()
    print("Data has been Loaded")
    print(greenf + "Welcome Back !, ", username, bluef)
    cli = bullet.YesNo(prompt = "Shall we Begin? ")
    result = cli.launch()
    if (result == True) :
        print(greenf + "Roger That !")
    else :
        print(redf + "Thanks for using regex-tutor!\n", end="")
        exit()

loadData()
Main()
